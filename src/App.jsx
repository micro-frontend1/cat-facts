import { useState } from "react";
import "./App.css";

function App(props) {
  const [products] = useState(props["products"]);

  return (
    <div className="product-list">
      {products.map((product) => (
        <section className="product" key={product.id}>
          <p>{product.name}</p>
          <button
            onClick={() =>
              new CustomEvent("purchase", { bubbles: true, detail: product })
            }
          >
            Buy Product
          </button>
        </section>
      ))}
    </div>
  );
}

export default App;
