import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";

/**
 * Web component in charge of managing the CatFacts React App
 * @example <product-list></product-list>
 */
class ProductList extends HTMLElement {
  constructor() {
    super();
    // Observes and updates the component when the attribute changes
    this.observer = new MutationObserver(() => this.update());
    this.observer.observe(this, { attributes: true });
  }

  /**
   * When the component is initialized it mounts it
   */
  connectedCallback() {
    this.mount();
  }

  /**
   * When the component is not needed it unmounts the component and disconnects the observer
   */
  disconnectedCallback() {
    this.unmount();
    this.observer.disconnect();
  }

  /**
   * Unmounts and remounts the component
   */
  update() {
    this.unmount();
    this.mount();
  }

  /**
   * Creates an element for the React app to be mounted on
   */
  mount() {
    const props = {
      ...this.getProps(this.attributes, App.propTypes),
    };

    this.innerHTML = `
      <div id="product-list"></div>
    `;

    ReactDOM.createRoot(document.getElementById("product-list")).render(
      <React.StrictMode>
        <App {...props} />
      </React.StrictMode>
    );
  }

  /**
   * Removes React app from the DOM
   */
  unmount() {
    ReactDOM.unmountComponentAtNode(this);
  }

  /**
   * Retrieves props from the web component
   * @param {object} attributes - The attributes assigned to the web component
   * @param {object} propTypes - Runtime type checking for React props and similar objects
   * @returns {object}
   */
  getProps(attributes, propTypes) {
    propTypes = propTypes || {};
    return [...attributes]
      .filter((attr) => attr.name !== "style")
      .map((attr) => this.convert(propTypes, attr.name, attr.value))
      .reduce((props, prop) => ({ ...props, [prop.name]: prop.value }), {});
  }

  /**
   * Converts web component attributes from strings to their corresponding value
   * @param {object} propTypes - Runtime type checking for React props and similar objects
   * @param {string} attrName
   * @param {string } attrValue
   * @returns {object}
   */
  convert(propTypes, attrName, attrValue) {
    const propName = Object.keys(propTypes).find(
      (key) => key.toLowerCase() == attrName
    );

    let value = attrValue;
    if (attrValue === "true" || attrValue === "false")
      value = attrValue == "true";
    else if (!isNaN(attrValue) && attrValue !== "") value = +attrValue;
    //        Matches Objects           Matches Arrays
    else if (/^{.*}/.exec(attrValue) || /^\[.*\]/.exec(attrValue))
      value = JSON.parse(attrValue);

    return {
      name: propName ? propName : attrName,
      value: value,
    };
  }
}

window.customElements.define("product-list", ProductList);
