import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import injectCSS from "vite-plugin-inject-css";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), injectCSS()],
  build: {
    lib: {
      entry: path.resolve(__dirname, "src/main.jsx"),
      name: "cat-facts",
      fileName: (format) => `cat-facts.${format}.js`,
    },
  },
});
